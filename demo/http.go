package main

/**
参考网址：https://gowebexamples.com/
 */
import (
	//http请求标准库
	"net/http"
	"fmt"
)

func main() {

	//注册一个请求的handler
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "Hello,you are requestd:%s\n", r.URL.Path)
	})
	//监听HTTP的连接以及端口号
	http.ListenAndServe(":8080", nil)
}
