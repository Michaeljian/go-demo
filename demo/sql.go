package main

import (
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
	"log"
	"fmt"
)
/**
参考文章：https://studygolang.com/articles/3022
 */
func main() {

	db,err :=sql.Open("mysql","root:123456@/test")
	if err !=nil{
		panic(err.Error())
	}
	defer db.Close()
	userId :=1
	stmt,err :=db.Prepare("select * from user where user_id = ?")

	rows,err :=stmt.Query(userId)
	defer rows.Close();
	for rows.Next() {
		var user_id int
		var user_name string
		if err := rows.Scan(&user_id,&user_name); err != nil {
			log.Fatal(err)
		}
		fmt.Print("name:%s\n",user_name)
		}
	log.Fatal("连接成功！")
}
