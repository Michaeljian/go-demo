package main

import (
	"github.com/gorilla/mux"
	"net/http"
	"fmt"
)

func main() {
	/**
	r.HandleFunc("/books/{title}", CreateBook).Methods("POST")
	r.HandleFunc("/books/{title}", ReadBook).Methods("GET")
	r.HandleFunc("/books/{title}", UpdateBook).Methods("PUT")
	r.HandleFunc("/books/{title}", DeleteBook).Methods("DELETE")
	 */
	 //创建一个路由
	r := mux.NewRouter()
	//请求的路径匹配，默认是Get请求
	r.HandleFunc("/books/{title}/page/{page}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		title := vars["title"]
		page := vars["page"]
		fmt.Fprint(w, "you are requested the book :"+title+"%s on page :"+page+"%s\n")
	})

	http.ListenAndServe(":8080", r)
}
